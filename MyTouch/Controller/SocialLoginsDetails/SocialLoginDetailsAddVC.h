//
//  SocialLoginDetailsAddVC.h
//  MyTouch
//
//  Created by volivesolutions on 13/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialLoginDetailsAddVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *mailIdTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)addBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addBtn_Outlet;
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
