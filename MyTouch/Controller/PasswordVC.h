//
//  PasswordVC.h
//  MyTouch
//
//  Created by volivesolutions on 16/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordVC : UIViewController
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *TF1;
@property (weak, nonatomic) IBOutlet UITextField *TF2;
@property (weak, nonatomic) IBOutlet UITextField *TF3;
@property (weak, nonatomic) IBOutlet UITextField *TF4;
@property (weak, nonatomic) IBOutlet UITextField *TF5;
@property (weak, nonatomic) IBOutlet UITextField *TF6;
@property (weak, nonatomic) IBOutlet UIView *backView;
- (IBAction)authenticateBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *authenticateBtn_Outlet;
@property NSString *passwordString;

@end
