//
//  ViewController.h
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIButton *touchIdBtn_Outlet;

- (IBAction)touchIdBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *IdImageView;


@end

