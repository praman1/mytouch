//
//  ViewController.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "ViewController.h"
#import "ActionListViewController.h"
#import "PasswordVC.h"
#import <LocalAuthentication/LocalAuthentication.h>
@interface ViewController ()
{
    UIAlertController *alertController;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
   // [_IdImageView.layer setShadowColor:[[UIColor colorWithRed:255.0/255.0 green:10.0/255.0 blue:80.0/255.0 alpha:1.0]CGColor]];
    [_IdImageView.layer setShadowColor:[[UIColor whiteColor]CGColor]];
    [_IdImageView.layer setShadowOffset:CGSizeMake(0,0)];
    [_IdImageView.layer setShadowRadius:20.0];
    [_IdImageView.layer setShadowOpacity:5.0];
    //_IdImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    //_IdImageView.layer.cornerRadius = 30;
   // _IdImageView.layer.borderWidth = 1.2f;
    _IdImageView.layer.masksToBounds = NO;
    
    LAContext *myContext = [[LAContext alloc] init];
    myContext.localizedFallbackTitle = @"";
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"Unlock MyTouchID to get into it";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:myLocalizedReasonString
            reply:^(BOOL success, NSError *error) {
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
            ActionListViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionListViewController"];
                   
                [self.navigationController pushViewController:home animated:YES];
                });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Else 1");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Authenticate!"
            message:@"Please unlock MyTouchID to continue" preferredStyle:UIAlertControllerStyleAlert];
                                        
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                       handler:nil];
            UIAlertAction *password = [UIAlertAction actionWithTitle:@"Enter Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                PasswordVC *password = [self.storyboard instantiateViewControllerWithIdentifier:@"PasswordVC"];
                [self.navigationController pushViewController:password animated:YES];
                }] ;
                [alert addAction:password];
                [alert addAction:cancel];
                                        
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
    }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Else 2");
        });
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0 animations:^{
            
            self.IdImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            [UIView animateWithDuration:0.4 animations:^{
                
                self.IdImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
            }];
            
        }];
        
    });
}

- (IBAction)touchIdBtn_Action:(id)sender {
    
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"Unlock MyTouchID";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:myLocalizedReasonString
                reply:^(BOOL success, NSError *error) {
                    if (success) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            ActionListViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionListViewController"];
                            [self.navigationController pushViewController:home animated:YES];
                                        
                        });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Authenticate!"
               message:@"Please unlock MyTouchID to continue" preferredStyle:UIAlertControllerStyleAlert];
                                        
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                           handler:nil];
                UIAlertAction *password = [UIAlertAction actionWithTitle:@"Enter Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                    PasswordVC *password = [self.storyboard instantiateViewControllerWithIdentifier:@"PasswordVC"];
                    [self.navigationController pushViewController:password animated:YES];
                    
                }] ;
               
                [alert addAction:cancel];
                [alert addAction:password];
                                        
                [self presentViewController:alert animated:YES completion:nil];
                                        
                });
            }
    }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{

        });
    }
    
}
@end
