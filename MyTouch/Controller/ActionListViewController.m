//
//  ActionListViewController.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "ActionListViewController.h"
#import "PersonalIdsViewController.h"
#import "SocialLoginsViewController.h"
#import "DebitCardDetailsViewController.h"
#import "BankAccountsViewController.h"
#import "ViewController.h"

@interface ActionListViewController ()

@end

@implementation ActionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0 animations:^{
            
            self.touchIdImageview.transform = CGAffineTransformMakeScale(0.01, 0.01);
            self.idStackView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            [UIView animateWithDuration:0.4 animations:^{
                
                self.touchIdImageview.transform = CGAffineTransformIdentity;
                self.idStackView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
            }];
            
        }];
        
    });
 
    
    [_touchIdImageview.layer setShadowColor:[[UIColor whiteColor]CGColor]];
    [_touchIdImageview.layer setShadowOffset:CGSizeMake(0,0)];
    [_touchIdImageview.layer setShadowRadius:20.0];
    [_touchIdImageview.layer setShadowOpacity:5.0];
    _touchIdImageview.layer.masksToBounds = NO;
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Back"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    barButtonItem.tintColor = [UIColor whiteColor];
  
  
}

-(void)goBack
{
    if ([_CheckString isEqualToString:@"password"]) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [[self navigationController] popToRootViewControllerAnimated:TRUE];
       // [self.navigationController popToRootViewControllerAnimated:TRUE];
    }else{
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [[self navigationController] popViewControllerAnimated:TRUE];
    }
    
}

- (IBAction)personalDetailsBtn_Action:(id)sender {
    
    PersonalIdsViewController *personal = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalIdsViewController"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: personal animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
   // [self.navigationController pushViewController:personal animated:TRUE];
}

- (IBAction)socialLoginBtn_Action:(id)sender {
    SocialLoginsViewController *social = [self.storyboard instantiateViewControllerWithIdentifier:@"SocialLoginsViewController"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: social animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
   // [self.navigationController pushViewController:personal animated:TRUE];
}

- (IBAction)debitCardDetialsBtn_Action:(id)sender {
    DebitCardDetailsViewController *debit = [self.storyboard instantiateViewControllerWithIdentifier:@"DebitCardDetailsViewController"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: debit animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
   // [self.navigationController pushViewController:personal animated:TRUE];
}

- (IBAction)bankAccountDetailsBtn_Action:(id)sender {
    BankAccountsViewController *bank = [self.storyboard instantiateViewControllerWithIdentifier:@"BankAccountsViewController"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: bank animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
   // [self.navigationController pushViewController:personal animated:TRUE];
}
- (IBAction)backBtn_Action:(id)sender {
    
    if ([_CheckString isEqualToString:@"password"]) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [[self navigationController] popToRootViewControllerAnimated:TRUE];
        // [self.navigationController popToRootViewControllerAnimated:TRUE];
    }else{
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [[self navigationController] popViewControllerAnimated:TRUE];
    }
}
@end
