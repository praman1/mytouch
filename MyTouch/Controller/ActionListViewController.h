//
//  ActionListViewController.h
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *touchIdImageview;

@property (weak, nonatomic) IBOutlet UIStackView *idStackView;

- (IBAction)personalDetailsBtn_Action:(id)sender;
- (IBAction)socialLoginBtn_Action:(id)sender;
- (IBAction)debitCardDetialsBtn_Action:(id)sender;
- (IBAction)bankAccountDetailsBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;

@property NSString *CheckString;
- (IBAction)backBtn_Action:(id)sender;

@end
