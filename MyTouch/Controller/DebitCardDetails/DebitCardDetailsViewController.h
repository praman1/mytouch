//
//  DebitCardDetailsViewController.h
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebitCardDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *debitCardTableview;
- (IBAction)backBtn_Action:(id)sender;
- (IBAction)addBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
