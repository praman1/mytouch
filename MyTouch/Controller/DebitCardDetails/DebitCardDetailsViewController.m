//
//  DebitCardDetailsViewController.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "DebitCardDetailsViewController.h"
#import "DebitCardDetailsAddVC.h"
#import "AppDelegate.h"
#import "TableviewCell.h"

API_AVAILABLE(ios(10.0))
@interface DebitCardDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    TableviewCell *debitCell;
    NSMutableArray *bankNameArray;
    NSMutableArray *cardNoArray;
    NSMutableArray *expiryDateArray;
    NSMutableArray *cvvPinArray;
    AppDelegate *appDelegate;
    
    NSMutableArray *dataArray;
    NSManagedObject *selectedDataObject;
    UIAlertController *alertController;
}

@end

@implementation DebitCardDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
    if (@available(iOS 10.0, *)) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    } else {
        // Fallback on earlier versions
    }
    bankNameArray = [[NSMutableArray alloc]initWithObjects:@"YES BANK",@"SBI BANK",@"SC BANK", nil];
    cardNoArray = [[NSMutableArray alloc]initWithObjects:@"465465454",@"454547987987",@"3654445", nil];
    expiryDateArray = [[NSMutableArray alloc]initWithObjects:@"20/20",@"30/30",@"12/12", nil];
    cvvPinArray = [[NSMutableArray alloc]initWithObjects:@"565/6666",@"999/8585",@"666/5858", nil];
    
    self.title = @"Debit Card Details";

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self myDataFetch];
}

-(void)myDataFetch
{
    
    
    if (@available(iOS 10.0, *)) {
        appDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    } else {
        // Fallback on earlier versions
    }
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"DebitCardDetails"];
    dataArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [_debitCardTableview reloadData];
    
    
}
-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectModel *managedObjectModel =
        [[context persistentStoreCoordinator] managedObjectModel];
        NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"DebitCardDetails"];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        [context deleteObject:[dataArray objectAtIndex:indexPath.row]];
        
        NSLog(@"object deleted");
        
        if (![context save:&error])
        {
            NSLog(@"Error deleting  - error:%@",error);
        }
        
        [dataArray removeObjectAtIndex:indexPath.row];
        [_debitCardTableview deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_debitCardTableview setEditing:NO animated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];
    selectedDataObject=obj;
    
    alertController = [UIAlertController alertControllerWithTitle:@"Edit Details"
                                                          message:@"You Can Edit Details"
                                                   preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField1) {

        textField1.text=[obj valueForKey:@"bankName"];
        textField1.placeholder =@"Bank Name";
        textField1.textColor = [UIColor blackColor];
        textField1.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField1.borderStyle = UITextBorderStyleRoundedRect;
        textField1.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField2) {
        textField2.text=[obj valueForKey:@"cardNumber"];
        textField2.placeholder =@"Card Number";
        textField2.textColor = [UIColor blackColor];
        textField2.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField2.borderStyle = UITextBorderStyleRoundedRect;
        textField2.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField3) {
        textField3.text=[obj valueForKey:@"expirydate"];
        textField3.placeholder =@"Expiry Date";
        textField3.textColor = [UIColor blackColor];
        textField3.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField3.borderStyle = UITextBorderStyleRoundedRect;
        textField3.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField4) {
        textField4.text=[obj valueForKey:@"cvv_pin"];
        textField4.placeholder =@"CVV&Pin";
        textField4.textColor = [UIColor blackColor];
        textField4.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField4.borderStyle = UITextBorderStyleRoundedRect;
        textField4.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = self->alertController.textFields;
        
        UITextField * bankNameTF = textfields[0];
        UITextField * cardNoTF = textfields[1];
        UITextField * expirydateTF = textfields[2];
        UITextField * cvv_pinTF = textfields[3];
        //_emailID.text = emailString;
        NSLog(@"bankNameTF %@",bankNameTF);
        NSLog(@"cardNoTF %@",cardNoTF);
        NSLog(@"expirydateTF %@",expirydateTF);
        NSLog(@"cvv_pinTF %@",cvv_pinTF);
        [self updateDataInDataBase:bankNameTF.text WithName:cardNoTF.text WithId:expirydateTF.text WithId:cvv_pinTF.text];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    alertController.view.tag = 222;
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)updateDataInDataBase:(NSString *)bankName WithName:(NSString *)cardNo WithId:(NSString *)expirydate WithId:(NSString *)cvv_pin
{
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectModel *managedObjectModel =
    [[context persistentStoreCoordinator] managedObjectModel];
    NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"DebitCardDetails"];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    
    [selectedDataObject setValue:bankName forKey:@"bankName"];
    [selectedDataObject setValue:cardNo forKey:@"cardNumber"];
    [selectedDataObject setValue:expirydate forKey:@"expirydate"];
    [selectedDataObject setValue:cvv_pin forKey:@"cvv_pin"];
    NSLog(@"object edited %@",selectedDataObject);
    
    if (![context save:&error]) {
        NSLog(@"Error editing  - error:%@",error);
    }
    [_debitCardTableview reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    debitCell = [_debitCardTableview dequeueReusableCellWithIdentifier:@"cell"];
    
    if (debitCell == nil) {
        
        debitCell = [[TableviewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];
    debitCell.bankNameLabel.text = [obj valueForKey:@"bankName"];
    debitCell.cardNumberLabel.text = [obj valueForKey:@"cardNumber"];
    debitCell.expirydateLabel.text = [obj valueForKey:@"expirydate"];
    debitCell.cvvPinLabel.text = [obj valueForKey:@"cvv_pin"];
  
    return debitCell;
}

- (IBAction)backBtn_Action:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

- (IBAction)addBtn_Action:(id)sender {
    DebitCardDetailsAddVC *addDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"DebitCardDetailsAddVC"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: addDetails animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
}
@end
