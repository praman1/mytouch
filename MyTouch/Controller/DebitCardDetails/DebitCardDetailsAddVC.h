//
//  DebitCardDetailsAddVC.h
//  MyTouch
//
//  Created by volivesolutions on 13/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebitCardDetailsAddVC : UIViewController
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *banknameTF;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *startEndDateTF;
@property (weak, nonatomic) IBOutlet UITextField *cvvPinTF;
- (IBAction)addBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
