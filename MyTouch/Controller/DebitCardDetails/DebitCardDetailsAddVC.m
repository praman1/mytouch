//
//  DebitCardDetailsAddVC.m
//  MyTouch
//
//  Created by volivesolutions on 13/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import "DebitCardDetailsAddVC.h"
#import "AppDelegate.h"

API_AVAILABLE(ios(10.0))
@interface DebitCardDetailsAddVC ()
{
    
    NSMutableArray *myDataArray;
    AppDelegate *appDelegate;
}

@end

@implementation DebitCardDetailsAddVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
    if (@available(iOS 10.0, *)) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    } else {
        // Fallback on earlier versions
    }
   
    _addBtn_Outlet.layer.shadowColor = [[UIColor whiteColor] CGColor];
    _addBtn_Outlet.layer.shadowOffset = CGSizeMake(0, 0);
    _addBtn_Outlet.layer.shadowOpacity = 20.0f;
    _addBtn_Outlet.layer.shadowRadius = 5.0f;
    _addBtn_Outlet.layer.masksToBounds = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];

    // Do any additional setup after loading the view.
}

- (IBAction)backBtn_Action:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

- (IBAction)addBtn_Action:(id)sender {
    
    if (_banknameTF.text.length != 0 && _cardNumberTF.text.length !=0 && _startEndDateTF.text.length !=0 && _cvvPinTF.text.length !=0) {
        
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        NSManagedObjectModel *managedObjectModel =
        [[context persistentStoreCoordinator] managedObjectModel];
        NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"DebitCardDetails"];
        NSManagedObject *debitCardDetailsDetailsObject = [[NSManagedObject alloc]
                                                     initWithEntity:entity insertIntoManagedObjectContext:context];

        [debitCardDetailsDetailsObject setValue:_banknameTF.text forKey:@"bankName"];
        [debitCardDetailsDetailsObject setValue:_cardNumberTF.text forKey:@"cardNumber"];
        [debitCardDetailsDetailsObject setValue:_startEndDateTF.text forKey:@"expirydate"];
        [debitCardDetailsDetailsObject setValue:_cvvPinTF.text forKey:@"cvv_pin"];
        [appDelegate saveContext];
        NSLog(@"debitCardDetailsObject is %@",debitCardDetailsDetailsObject);
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [[self navigationController] popViewControllerAnimated:NO];

        //[self.navigationController popViewControllerAnimated:TRUE];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hello!"
                                    
                                                                       message:@"Enter all the details" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                         }];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [_banknameTF resignFirstResponder];
    [_cardNumberTF resignFirstResponder];
    [_startEndDateTF resignFirstResponder];
    [_cvvPinTF resignFirstResponder];
    
}
@end
