//
//  PersonalDetailsAddVC.h
//  MyTouchID
//
//  Created by volivesolutions on 06/07/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol myDataSending <NSObject>;

@required

-(void)myData:(NSMutableDictionary *)dataDict;
@end

@interface PersonalDetailsAddVC : UIViewController

@property(strong,nonatomic)id<myDataSending>delegate;

- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *typeOfIdTF;
@property (weak, nonatomic) IBOutlet UITextField *nameOnCardTF;
@property (weak, nonatomic) IBOutlet UITextField *idNumberTF;
@property (weak, nonatomic) IBOutlet UIButton *addBtn_Outlet;
- (IBAction)addBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
