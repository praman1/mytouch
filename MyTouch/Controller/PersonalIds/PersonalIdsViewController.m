//
//  PersonalIdsViewController.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "PersonalIdsViewController.h"
#import "PersonalDetailsAddVC.h"
#import "TableviewCell.h"
#import "AppDelegate.h"

API_AVAILABLE(ios(10.0))
@interface PersonalIdsViewController ()<UITableViewDelegate,UITableViewDataSource,myDataSending>
{
    NSMutableArray *IDTypeArray;
    NSMutableArray *nameArray;
    NSMutableArray *IDNumberArray;
    TableviewCell *personalCell;
    AppDelegate *dataAppDelegate;
    NSMutableArray *dataArray;
    NSManagedObject *selectedDataObject;
    UIAlertController *alertController;
}

@end

@implementation PersonalIdsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    dataArray = [NSMutableArray new];
    if (@available(iOS 10.0, *)) {
        dataAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    } else {
        // Fallback on earlier versions
    }
    self.title = @"Personal IDs Details";
    [self myDataFetch];
}

-(void)myData:(NSMutableDictionary *)dataDict
{
    IDTypeArray = [NSMutableArray new];
    nameArray = [NSMutableArray new];
    IDNumberArray = [NSMutableArray new];
    
    NSLog(@"Data Dict is %@",dataDict);
    [IDTypeArray addObject:[dataDict objectForKey:@"type"]];
    [nameArray addObject:[dataDict objectForKey:@"name"]];
    [IDNumberArray addObject:[dataDict objectForKey:@"number"]];
    [_personalIdTableview reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self myDataFetch];
}
-(void)myDataFetch
{
    if (@available(iOS 10.0, *)) {
        dataAppDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    } else {
        // Fallback on earlier versions
    }
        NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"PersonalIdDetails"];
        dataArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];

        [_personalIdTableview reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    personalCell = [_personalIdTableview dequeueReusableCellWithIdentifier:@"cell"];
    
    if(personalCell == nil)
    {
        personalCell = [[TableviewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
        NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];
        personalCell.IDTypeLabel.text = [obj valueForKey:@"typeOfId"];
        personalCell.nameLabel.text   = [obj valueForKey:@"nameOnCard"];
        personalCell.numberLabel.text = [obj valueForKey:@"idNumber"];
    
    return personalCell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        
        NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectModel *managedObjectModel =
        [[context persistentStoreCoordinator] managedObjectModel];
        NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"PersonalIdDetails"];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        [context deleteObject:[dataArray objectAtIndex:indexPath.row]];
        
        NSLog(@"object deleted");
        
        if (![context save:&error])
        {
            NSLog(@"Error deleting  - error:%@",error);
        }
        
        [dataArray removeObjectAtIndex:indexPath.row];
        [_personalIdTableview deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_personalIdTableview setEditing:NO animated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];
    selectedDataObject=obj;
    
    alertController = [UIAlertController alertControllerWithTitle:@"Edit Details"
                                                          message:@"You Can Edit Details"
                                                   preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField1) {
        
        textField1.text=[obj valueForKey:@"typeOfId"];
        textField1.placeholder =@"Type Of Id";
        textField1.textColor = [UIColor blackColor];
        textField1.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField1.borderStyle = UITextBorderStyleRoundedRect;
        textField1.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField2) {
        textField2.text=[obj valueForKey:@"nameOnCard"];
        textField2.placeholder =@"Name On Card";
        textField2.textColor = [UIColor blackColor];
        textField2.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField2.borderStyle = UITextBorderStyleRoundedRect;
        textField2.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField3) {
        textField3.text=[obj valueForKey:@"idNumber"];
        textField3.placeholder =@"Id Number";
        textField3.textColor = [UIColor blackColor];
        textField3.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField3.borderStyle = UITextBorderStyleRoundedRect;
        textField3.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = self->alertController.textFields;
        
        UITextField * typeOfIdTF = textfields[0];
        UITextField * nameOnCardTF = textfields[1];
        UITextField * idNumberTF = textfields[2];
       
        NSLog(@"typeOfIdTF %@",typeOfIdTF);
        NSLog(@"nameOnCardTF %@",nameOnCardTF);
        NSLog(@"idNumberTF %@",idNumberTF);
        [self updateDataInDataBase:typeOfIdTF.text WithName:nameOnCardTF.text WithId:idNumberTF.text];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    alertController.view.tag = 222;
    [self presentViewController:alertController animated:YES completion:nil];

}

-(void)updateDataInDataBase:(NSString *)typeOfId WithName:(NSString *)nameOnCard WithId:(NSString *)idNumber
{
    
    NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectModel *managedObjectModel =
    [[context persistentStoreCoordinator] managedObjectModel];
    NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"PersonalIdDetails"];
    [fetchRequest setEntity:entity];
    
    NSError *error;

    [selectedDataObject setValue:typeOfId forKey:@"typeOfId"];
    [selectedDataObject setValue:nameOnCard forKey:@"nameOnCard"];
    [selectedDataObject setValue:idNumber forKey:@"idNumber"];
    NSLog(@"object edited %@",selectedDataObject);
    
    if (![context save:&error]) {
        NSLog(@"Error editing  - error:%@",error);
    }
    [_personalIdTableview reloadData];
}

- (IBAction)backBtn_Action:(id)sender {
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
   // [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)addBtn_Action:(id)sender {
    
    PersonalDetailsAddVC *addDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsAddVC"];
   // personalAdd.delegate = self;
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: addDetails animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
}

@end
