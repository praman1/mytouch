//
//  PasswordVC.m
//  MyTouch
//
//  Created by volivesolutions on 16/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import "PasswordVC.h"
#import "ActionListViewController.h"
#define RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface PasswordVC ()

@end

@implementation PasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [_TF1 becomeFirstResponder];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
   // gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
     gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
    _authenticateBtn_Outlet.layer.borderColor = [[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0] CGColor] ;
    _authenticateBtn_Outlet.layer.borderWidth = 1.0f;
    
    
    _authenticateBtn_Outlet.layer.shadowColor = [[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0] CGColor];
    _authenticateBtn_Outlet.layer.shadowOffset = CGSizeMake(0, 3);
    _authenticateBtn_Outlet.layer.shadowOpacity = 1.0f;
    _authenticateBtn_Outlet.layer.shadowRadius = 0.3f;
    _authenticateBtn_Outlet.layer.masksToBounds = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
}

-(void)passwordValidation
{
    _passwordString = [NSString stringWithFormat:@"%@%@%@%@%@%@",_TF1.text,_TF2.text,_TF3.text,_TF4.text,_TF5.text,_TF6.text];
    
    if (_TF6.text.length > 0) {
        if ([_passwordString isEqualToString:@"200693"]) {
            
            ActionListViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionListViewController"];
            home.CheckString = @"password";
            [UIView  beginAnimations: @"Showinfo"context: nil];
            [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.75];
            [self.navigationController pushViewController: home animated:NO];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
            [UIView commitAnimations];
           // [self.navigationController pushViewController:home animated:YES];
        }
        else{
            
            [self textfieldAnimate];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Incorrect Password!"
                                                                           message:@"Please check the password you have entered" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           self.TF1.text = @"";
                                                           self.TF2.text = @"";
                                                           self.TF3.text = @"";
                                                           self.TF4.text = @"";
                                                           self.TF5.text = @"";
                                                           self.TF6.text = @"";
                                                       }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
}

-(void)textfieldAnimate
{
    CGAffineTransform leftWobble = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(-10.0));
    CGAffineTransform rightWobble = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(10.0));
    
    _TF1.transform = leftWobble;
    _TF2.transform = leftWobble;
    _TF3.transform = leftWobble;
    _TF4.transform = leftWobble;
    _TF5.transform = leftWobble;
    _TF6.transform = leftWobble;
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:(UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse) animations:^{
                            
                            [UIView setAnimationRepeatCount:6]; // Change this value as you want
                            
                            self.TF1.transform = rightWobble;
                            self.TF2.transform = rightWobble;
                            self.TF3.transform = rightWobble;
                            self.TF4.transform = rightWobble;
                            self.TF5.transform = rightWobble;
                            self.TF6.transform = rightWobble;
                            
                        } completion:^(BOOL finished){
                            
                            self.TF1.transform = CGAffineTransformIdentity;
                            self.TF2.transform = CGAffineTransformIdentity;
                            self.TF3.transform = CGAffineTransformIdentity;
                            self.TF4.transform = CGAffineTransformIdentity;
                            self.TF5.transform = CGAffineTransformIdentity;
                            self.TF6.transform = CGAffineTransformIdentity;
                            
                        }];
}

- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString* )string
{
    NSString *string1 = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSUInteger newLength = string1.length;
   
    if (newLength == 1) {
        if (textField == _TF1)
        {
            _TF1.text = string1;
            [_TF2 becomeFirstResponder];
            
            return false;
        }
        else if (textField == _TF2)
        {
            _TF2.text = string1;
            [_TF3 becomeFirstResponder];
            
            return false;
        }
        else if (textField == _TF3)
        {
            _TF3.text = string1;
            [_TF4 becomeFirstResponder];
            
            return false;
        }
        else if (textField == _TF4)
        {
            _TF4.text = string1;
            [_TF5 becomeFirstResponder];
            
            return false;
        }
        else if (textField == _TF5)
        {
            _TF5.text = string1;
            [_TF6 becomeFirstResponder];
            
            return false;
        }
        else if (textField == _TF6)
        {
            _TF6.text = string1;
            [_TF6 resignFirstResponder];
            
            [self passwordValidation];
            
            return true;
        }
    }
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [_TF1 resignFirstResponder];
    [_TF2 resignFirstResponder];
    [_TF3 resignFirstResponder];
    [_TF4 resignFirstResponder];
    [_TF5 resignFirstResponder];
    [_TF6 resignFirstResponder];
}

- (IBAction)backBtn_Action:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

- (IBAction)authenticateBtn_Action:(id)sender {
    
}

@end
