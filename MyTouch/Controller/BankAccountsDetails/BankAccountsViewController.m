//
//  BankAccountsViewController.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "BankAccountsViewController.h"
#import "BankAccountsAddVC.h"
#import "TableviewCell.h"
#import "AppDelegate.h"
API_AVAILABLE(ios(10.0))
@interface BankAccountsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    TableviewCell *bankCell;
   
    NSMutableArray *bankNameArray;
    NSMutableArray *accountNumberArray;
    NSMutableArray *IFSCCodeArray;
    NSMutableArray *branchNameArray;
    NSMutableArray *nebankingUsernameArray;
    NSMutableArray *passwordArray;
    
    AppDelegate *dataAppDelegate;
    NSMutableArray *dataArray;
    NSManagedObject *selectedDataObject;
    UIAlertController *alertController;
}

@end

@implementation BankAccountsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    
   // gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:225 / 255.0 blue:255 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0 / 255.0 green:240.0 / 255.0 blue:179 / 255.0 alpha:1.0].CGColor];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 / 255.0 green:89.0 / 255.0 blue:237.0 / 255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 / 255.0 green:212.0 / 255.0 blue:229.0 / 255.0 alpha:1.0].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 1);
    gradient.frame = _backView.bounds;
    [_backView.layer insertSublayer:gradient atIndex:0];
    
    dataArray = [NSMutableArray new];
    if (@available(iOS 10.0, *)) {
        dataAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    } else {
        // Fallback on earlier versions
    }
    [self myDataFetch];
 
    self.title = @"Bank Accounts Details";

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self myDataFetch];
}

-(void)myDataFetch
{
    
    
    if (@available(iOS 10.0, *)) {
        dataAppDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    } else {
        // Fallback on earlier versions
    }
    NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BankAccountDetails"];
    dataArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [_bankAccontsTableview reloadData];
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    bankCell = [_bankAccontsTableview dequeueReusableCellWithIdentifier:@"cell"];
    
    if (bankCell == nil) {
        
        bankCell = [[TableviewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];

    bankCell.bankNameLabel.text = [obj valueForKey:@"bankName"];
    bankCell.accountNumberLabel.text = [obj valueForKey:@"accountNumber"];
    bankCell.IFSCCodeLabel.text = [obj valueForKey:@"ifscCode"];
    bankCell.branchNameLabel.text = [obj valueForKey:@"branchName"];
    bankCell.userNameLabel.text = [obj valueForKey:@"netbankingUN"];
    bankCell.netPasswordLabel.text = [obj valueForKey:@"password"];
    
    return bankCell;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        
        NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectModel *managedObjectModel =
        [[context persistentStoreCoordinator] managedObjectModel];
        NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"BankAccountDetails"];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        [context deleteObject:[dataArray objectAtIndex:indexPath.row]];
        
        NSLog(@"object deleted");
        
        if (![context save:&error])
        {
            NSLog(@"Error deleting  - error:%@",error);
        }
        
        [dataArray removeObjectAtIndex:indexPath.row];
        [_bankAccontsTableview deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_bankAccontsTableview setEditing:NO animated:YES];
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *obj=[dataArray objectAtIndex:indexPath.row ];
    selectedDataObject=obj;
    
    alertController = [UIAlertController alertControllerWithTitle:@"Edit Details"
                                                          message:@"You Can Edit Details"
                                                   preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField1) {

        textField1.text=[obj valueForKey:@"bankName"];
        textField1.placeholder =@"Bank Name";
        textField1.textColor = [UIColor blackColor];
        textField1.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField1.borderStyle = UITextBorderStyleRoundedRect;
        textField1.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField2) {
        textField2.text=[obj valueForKey:@"accountNumber"];
        textField2.placeholder =@"Account Number";
        textField2.textColor = [UIColor blackColor];
        textField2.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField2.borderStyle = UITextBorderStyleRoundedRect;
        textField2.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField3) {
        textField3.text=[obj valueForKey:@"ifscCode"];
        textField3.placeholder =@"IFSC Code";
        textField3.textColor = [UIColor blackColor];
        textField3.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField3.borderStyle = UITextBorderStyleRoundedRect;
        textField3.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField4) {
        textField4.text=[obj valueForKey:@"branchName"];
        textField4.placeholder =@"Branch Name";
        textField4.textColor = [UIColor blackColor];
        textField4.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField4.borderStyle = UITextBorderStyleRoundedRect;
        textField4.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField5) {
        textField5.text=[obj valueForKey:@"netbankingUN"];
        textField5.placeholder =@"Netbanking Username";
        textField5.textColor = [UIColor blackColor];
        textField5.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField5.borderStyle = UITextBorderStyleRoundedRect;
        textField5.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField6) {
        textField6.text=[obj valueForKey:@"password"];
        textField6.placeholder =@"Password";
        textField6.textColor = [UIColor blackColor];
        textField6.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField6.borderStyle = UITextBorderStyleRoundedRect;
        textField6.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = self->alertController.textFields;
 
        UITextField * bankNameTF = textfields[0];
        UITextField * accountNumberTF = textfields[1];
        UITextField * ifscCodeTF = textfields[2];
        UITextField * branchNameTF = textfields[3];
        UITextField * netbankingUNTF = textfields[4];
        UITextField * passwordTF = textfields[5];
        //_emailID.text = emailString;
        NSLog(@"bankNameTF %@",bankNameTF);
        NSLog(@"cardNoTF %@",accountNumberTF);
        NSLog(@"expirydateTF %@",ifscCodeTF);
        NSLog(@"cvv_pinTF %@",branchNameTF);
        NSLog(@"expirydateTF %@",netbankingUNTF);
        NSLog(@"cvv_pinTF %@",passwordTF);
        
        [self updateDataInDataBase:bankNameTF.text WithName:accountNumberTF.text WithId:ifscCodeTF.text WithId:branchNameTF.text WithId:netbankingUNTF.text WithId:passwordTF.text];
        //emailIdString=namefield.text;
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    alertController.view.tag = 222;
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)updateDataInDataBase:(NSString *)bankName WithName:(NSString *)acntNo WithId:(NSString *)ifscCode WithId:(NSString *)branchname WithId:(NSString *)netBankUN WithId:(NSString *)password
{
    
    NSManagedObjectContext *context = dataAppDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectModel *managedObjectModel =
    [[context persistentStoreCoordinator] managedObjectModel];
    NSEntityDescription *entity = [[managedObjectModel entitiesByName] objectForKey:@"BankAccountDetails"];
    [fetchRequest setEntity:entity];
    
    NSError *error;

    [selectedDataObject setValue:bankName forKey:@"bankName"];
    [selectedDataObject setValue:acntNo forKey:@"accountNumber"];
    [selectedDataObject setValue:ifscCode forKey:@"ifscCode"];
    [selectedDataObject setValue:branchname forKey:@"branchName"];
    [selectedDataObject setValue:netBankUN forKey:@"netbankingUN"];
    [selectedDataObject setValue:password forKey:@"password"];
    NSLog(@"object edited %@",selectedDataObject);
    
    if (![context save:&error]) {
        NSLog(@"Error editing  - error:%@",error);
    }
    [_bankAccontsTableview reloadData];
}


- (IBAction)backBtn_Action:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

- (IBAction)addBtn_Action:(id)sender {
    
    BankAccountsAddVC *addDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"BankAccountsAddVC"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController: addDetails animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
   // [self.navigationController pushViewController:addDetails animated:TRUE];
}
@end
