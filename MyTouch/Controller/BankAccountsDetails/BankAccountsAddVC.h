//
//  BankAccountsAddVC.h
//  MyTouch
//
//  Created by volivesolutions on 13/07/18.
//  Copyright © 2018 Prashanth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankAccountsAddVC : UIViewController
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTF;
@property (weak, nonatomic) IBOutlet UITextField *acNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *IFSCCodeTF;
@property (weak, nonatomic) IBOutlet UITextField *branchNameTF;
@property (weak, nonatomic) IBOutlet UITextField *netBankingUNTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *addBtn_Outlet;
- (IBAction)addBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@end
