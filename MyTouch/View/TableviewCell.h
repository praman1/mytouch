//
//  TableviewCell.h
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableviewCell : UITableViewCell

//***Personal Ids
@property (weak, nonatomic) IBOutlet UILabel *IDTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

//***Social Logins
@property (weak, nonatomic) IBOutlet UILabel *accountNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mailIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;

//***Debit Card Details
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *expirydateLabel;
@property (weak, nonatomic) IBOutlet UILabel *cvvPinLabel;

//***Bank Accounts
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *IFSCCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *netPasswordLabel;


@end
