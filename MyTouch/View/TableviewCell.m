//
//  TableviewCell.m
//  MyTouchID
//
//  Created by volive solutions on 17/02/18.
//  Copyright © 2018 volive solutions. All rights reserved.
//

#import "TableviewCell.h"

@implementation TableviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
